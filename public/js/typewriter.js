var app = document.getElementById('app');

var typewriter = new Typewriter(app, {
    loop: false,
    delay: 50,
    deleteSpeed: 50,
});

typewriter.typeString('Ultra bubble smashy comfort zone to appreciate yourself for your wins only.') 
    .pauseFor(2500)
    .deleteAll()
    .typeString('Naah...')
    .pauseFor(2500)
    .deleteAll()
    .typeString('Script your sins and/or good deeds. Point out your life achievements.')
    // .deleteChars(7)
    // .typeString('<strong>altered!</strong>')
    // .pauseFor(2500)
    .start();
