
var options = {
    allowTaint: false,
    width: "960px",
    height: "540px",
    foreignObjectRendering: false,
    imageTimeout: 300,
};

function myFunction() {

    var element = document.getElementById('capture');

    html2canvas(element).then(function(canvas) {
                        const base64image = canvas.toDataURL("image/png");
                                        window.location.href = base64image;
                                        allowTaint = false;
                                        width = "960px";
                                        height = "540px";
                                        foreignObjectRendering = false;
                                        imageTimeout = 300;
                                        backgroundColor = "#ffffff";
                                        canvas.setAttribute('id','canvasdownload');
        document.body.appendChild(canvas)
    });
  }